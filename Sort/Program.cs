﻿using System;

namespace Sort
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] data = {3, 19, 2, 3, 5, 67, 43, 545, 452, 23, 34321, -4, -3, -2, -1, 0, 1, 2, 3, 44};
            int lastCheckedElement = data.Length - 1;
            while (lastCheckedElement >= 0)
            {
                for (int i = 0; i < lastCheckedElement; i++)
                {
                    if (data[i] > data[i + 1])
                    {
                        (data[i + 1], data[i]) = (data[i], data[i + 1]);
                    }
                }

                lastCheckedElement--;
            }

            foreach (var el in data)
            {
                Console.Write($"{el} ");
            }
        }
    }
}